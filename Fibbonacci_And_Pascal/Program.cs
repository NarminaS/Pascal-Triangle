﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fibbonacci_And_Pascal
{
    class Program
    {
        static void Main(string[] args)
        {
            int floors = Int32.Parse(Console.ReadLine());
            int[][] tri = new int[floors][];
            int step = 1;
            for (int i = 0; i < tri.Length; i++)
            {
                // Define internal arrays
                tri[i] = new int[step];
                tri[i][0] = 1;
                tri[i][tri[i].Length - 1] = 1;
                step++;
                //Fill internal arrays
                for (int j = 1; j < tri[i].Length-1; j++)
                {
                    tri[i][j] = tri[i - 1][j - 1] + tri[i - 1][j];
                }
                //Print out the Pascal Triangle
                for (int k = 0; k < tri[i].Length; k++)
                {
                    Console.Write(tri[i][k] + " ");
                }

                Console.WriteLine();
            }
        }
    }
}
